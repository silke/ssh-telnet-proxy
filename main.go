package main

import (
	"flag"
	"log"
	"net"
	"path"
	"strings"

	"gitlab.com/silkeh/env"
)

const defaultKeys = "ssh_host_ecdsa_key," +
	"ssh_host_ed25519_key," +
	"ssh_host_rsa_key"

func main() {
	var keys, keyDir, addr, remote string

	flag.StringVar(&addr, "addr", ":2022", "Address to listen on")
	flag.StringVar(&keyDir, "key-dir", ".", "Path to directory containing SSH keys")
	flag.StringVar(&keys, "keys", defaultKeys, "Comma-separated list of SSH host keys")
	flag.StringVar(&remote, "remote", "nyancat.utwente.io:23", "Remote Telnet server to proxy")

	err := env.ParseWithFlags()
	if err != nil {
		log.Fatal("Error parsing flags: ", err)
	}

	keyList := strings.Split(keys, ",")
	for i, k := range keyList {
		keyList[i] = path.Join(keyDir, k)
	}

	server, err := NewSSHServer(keyList...)
	if err != nil {
		log.Fatal("Error creating SSH server: ", err)
	}

	listener, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatal("Error listening on connection: ", err)
	}

	log.Print("Listening on ", addr)
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Print("Error accepting connection: ", err)
			continue
		}

		log.Printf("New connection from %s", conn.RemoteAddr())

		telConn, err := DialTelnet(remote)
		if err != nil {
			log.Print("Error connecting to remote telnet server: ", err)
			continue
		}

		go func() {
			err = server.NewProxyConn(conn, telConn)
			if err != nil {
				log.Print("Error handling SSH connection: ", err)
			}
		}()
	}
}
