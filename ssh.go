package main

import (
	"encoding/binary"
	"fmt"
	"io"
	"io/ioutil"
	"net"

	"golang.org/x/crypto/ssh"
)

var config = ssh.ServerConfig{
	NoClientAuth: true,
	PasswordCallback: func(conn ssh.ConnMetadata, password []byte) (*ssh.Permissions, error) {
		return &ssh.Permissions{}, nil
	},
	ServerVersion: "SSH-2.0-Proxy_0.1",
}

type SSHServer struct {
	config *ssh.ServerConfig
}

func NewSSHServer(keys ...string) (*SSHServer, error) {
	server := &SSHServer{config: &config}

	for _, key := range keys {
		err := server.AddHostKeyFromFile(key)
		if err != nil {
			return nil, err
		}
	}

	return server, nil
}

func (s *SSHServer) AddHostKeyFromFile(file string) error {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return fmt.Errorf("cannot read key file %q: %s", file, err)
	}

	key, err := ssh.ParsePrivateKey(data)
	if err != nil {
		return fmt.Errorf("error parsing key file %q: %s", file, err)
	}

	s.config.AddHostKey(key)
	return nil
}

func (s *SSHServer) NewProxyConn(nConn net.Conn, telnet *Telnet) error {
	// Create the server connection
	conn, chans, reqs, err := ssh.NewServerConn(nConn, s.config)
	if err != nil {
		return err
	}
	defer conn.Close()

	// The incoming Request channel must be serviced.
	go ssh.DiscardRequests(reqs)

	// Service the incoming Channel channel.
	for newChannel := range chans {
		// Channels have a type, depending on the application level
		// protocol intended. In the case of a shell, the type is
		// "session" and ServerShell may be used to present a simple
		// terminal interface.
		if newChannel.ChannelType() != "session" {
			newChannel.Reject(ssh.UnknownChannelType, "unknown channel type")
			continue
		}

		// Accept the incoming channel
		channel, requests, err := newChannel.Accept()
		if err != nil {
			return err
		}

		// Sessions have out-of-band requests such as "shell",
		// "pty-req" and "env".  Here we handle only the
		// "shell" request.
		go func(in <-chan *ssh.Request) {
			for req := range in {
				switch req.Type {
				case "shell":
					req.Reply(true, nil)

				case "pty-req":
					term, _, size := parsePTYRequest(req.Payload)
					opts := []TelnetOption{
						&TelnetTerminalTypeOption{Term: string(term)},
						&TelnetWindowSizeOption{Width: uint16(size.Cols), Height: uint16(size.Rows)},
					}
					telnet.SendSubOption(opts...)

				case "window-change":
					size := NewPTYSize(req.Payload)
					telnet.SendSubOption(&TelnetWindowSizeOption{
						Width: uint16(size.Cols), Height: uint16(size.Rows)})

				default:
					req.Reply(false, nil)
				}
			}
		}(requests)

		// Connect the channel to the telnet client,
		// and vice versa
		go func() {
			defer channel.Close()
			go io.Copy(telnet, channel)
			io.Copy(channel, telnet)
		}()
	}

	return nil
}

func parsePTYRequest(b []byte) (term, modes []byte, size *PTYSize) {
	term = parseString(b)
	modes = parseString(b[len(term)+20:])
	size = NewPTYSize(b[len(term)+4 : len(term)+20])
	return
}

func parseString(b []byte) []byte {
	size := binary.BigEndian.Uint32(b[:4])
	return b[4 : size+4]
}

type PTYSize struct {
	Cols, Rows, Width, Height uint32
}

func NewPTYSize(b []byte) *PTYSize {
	return &PTYSize{
		Cols:   binary.BigEndian.Uint32(b[:4]),
		Rows:   binary.BigEndian.Uint32(b[4:8]),
		Width:  binary.BigEndian.Uint32(b[8:12]),
		Height: binary.BigEndian.Uint32(b[12:16]),
	}
}
