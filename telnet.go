package main

import (
	"encoding/binary"
	"net"
	"strings"
)

const (
	TelnetCommand      = 0xff
	TelnetSuboption    = 0xfa
	TelnetSuboptionEnd = 0xf0
	TelnetWindowSize   = 0x1f
	TelnetTerminalType = 0x18
)

type Telnet struct {
	net.Conn
}

func DialTelnet(addr string) (*Telnet, error) {
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		return nil, err
	}

	return &Telnet{Conn: conn}, nil
}

func (t *Telnet) SendSubOption(options ...TelnetOption) error {
	var data []byte
	for _, opt := range options {
		data = append(data, opt.Data()...)
	}

	_, err := t.Write(data)
	return err
}

type TelnetOption interface {
	Data() []byte
}

type TelnetWindowSizeOption struct {
	Width, Height uint16
}

func (o *TelnetWindowSizeOption) Data() []byte {
	cmd := make([]byte, 4)
	binary.BigEndian.PutUint16(cmd[0:2], o.Width)
	binary.BigEndian.PutUint16(cmd[2:4], o.Height)
	return marshalCommand(TelnetWindowSize, cmd)
}

type TelnetTerminalTypeOption struct {
	Term string
}

func (o *TelnetTerminalTypeOption) Data() []byte {
	term := strings.ToUpper(o.Term)
	return marshalCommand(TelnetTerminalType, []byte("\000"+term))
}

func marshalCommand(opt byte, b []byte) []byte {
	data := make([]byte, len(b)+5)
	copy(data[0:3], []byte{TelnetCommand, TelnetSuboption, opt})
	copy(data[3:], b)
	copy(data[len(b)+3:], []byte{TelnetCommand, TelnetSuboptionEnd})
	return data
}
