module gitlab.snt.utwente.nl/silkeh/ssh-telnet-proxy

go 1.18

require (
	gitlab.com/silkeh/env v0.1.0
	golang.org/x/crypto v0.0.0-20220525230936-793ad666bf5e
)

require golang.org/x/sys v0.0.0-20220608164250-635b8c9b7f68 // indirect
